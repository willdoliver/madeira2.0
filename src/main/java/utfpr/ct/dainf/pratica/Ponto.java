package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author Willdoliver
 */
public class Ponto {
    private double x;
    private double y;
    private double z;
    
    public Ponto(){
        x=0;
        y=0;
        z=0;
    }
    
    public Ponto(double x,double y,double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    //getters
    public double getX(){
        return x;
    }
    public double getY(){
        return y;
    }
    public double getZ(){
        return z;
    }
    //setters
    public void setX(double x){
        this.x = x;
    }
    public void setY(double y){
        this.y = y;
    }
    public void setZ(double z){
        this.z = z;
    }
    
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }
    
    public String toString() {
       return String.format(this.getNome() + "(%f,%f, %f)",x, y, z);
    }
    
    public boolean equals(Ponto a, Ponto b){
        if ((a.x == b.x) && (a.y == b.y) && (a.z == b.z)){
            return true;
        }else return false;
    }
    
    public double dist(Ponto a, Ponto b){
        return Math.sqrt(Math.pow(b.x-a.x,2) + Math.pow(b.y-a.y,2) + Math.pow(b.z-a.z,2));
    }
 
    abstract class Ponto2D extends Ponto{
        
    
    }
    
}
